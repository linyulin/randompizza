from turtle import *
import pprint
import random
#create pizza menu
cheese_pizza = {
        'size': "small",
        'shape': "square",
        'crust': "traditional",
        'toppings': "extra cheese",
        }
pepperoni_pizza = {
        'size': "large",
        'shape': "round",
        'crust': "deep dish",
        'toppings': "pepperoni",
        }
Hawaii_pizza = {
        'size': "small",
        'shape': "round",
        'crust': "traditional",
        'toppings': "pineapple",
        }
pizza_list = [Hawaii_pizza,cheese_pizza,pepperoni_pizza]

def print_pizza_order(pizza):
    write("I would like a " + pizza['size'] + " " + pizza['shape'] + " pizza on a " + pizza['crust'] + " crust ")
    penup()
    right(90)
    forward(30)
    left(90)
    pendown()
    write("With " + pizza['toppings' ] + " " + "on it")

drawpizza = random.choice(pizza_list)
print_pizza_order(drawpizza)
#draw shape
#penup()
backward(100)
pendown()

if drawpizza['toppings'] =="pepperoni":
    if drawpizza['shape'] == 'round':
        if drawpizza['size'] == "large":
            color("orange")
            begin_fill()
            circle(100)
            end_fill()

            pensize(10)
            circle(100)
            penup()
            goto(-100,-10)
            pendown()
            color("red")
            begin_fill()
            circle(20)
            end_fill()
            penup()
            goto(-100,100)
            pendown()
            begin_fill()
            circle(20)
            end_fill()
            penup()
            goto(-50,50)
            pendown()
            begin_fill()
            circle(20)
            end_fill()
            penup()
            goto(-150,50)
            pendown()
            begin_fill()
            circle(20)
            end_fill()

if drawpizza['toppings'] =="pineapple":
    if drawpizza['shape'] == 'round':
        if drawpizza['size'] == "small":
            color("orange")
            begin_fill()
            circle(50)
            end_fill()
            penup()
            goto(-100,-20)
            pendown()
            color("yellow")
            begin_fill()
            circle(10)
            end_fill()
            penup()
            goto(-100,25)
            pendown()
            begin_fill()
            circle(10)
            end_fill()
            penup()
            goto(-75,0)
            pendown()
            begin_fill()
            circle(10)
            end_fill()
            penup()
            goto(-125,0)
            pendown()
            begin_fill()
            circle(10)
            end_fill()
    if drawpizza['shape'] == 'square':
        if drawpizza['size'] =="small":
            backward(70)
            left(90)
            forward(150)
            right(90)
            forward(150)
            right(90)
            forward(150)

if drawpizza['toppings'] =="extra cheese":
    if drawpizza['shape']=="square":
        if drawpizza['size']=="small":
            backward(70)
            color("orange")
            begin_fill()
            pensize(1)
            left(90)
            forward(150)
            right(90)
            forward(150)
            right(90)
            forward(150)
            right(90)
            forward(150)
            end_fill()
            color("yellow")
            pensize(10)
            right(90)
            forward(150)
            right(90)
            forward(150)
            right(90)
            forward(150)
            right(90)
            forward(150)
            color("orange")
            begin_fill()
            pensize(1)
            right(90)
            forward(150)
            right(90)
            forward(150)
            right(90)
            forward(150)
            right(90)
            forward(150)
            end_fill()

done()
